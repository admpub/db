module github.com/webx-top/db

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.5.1
	github.com/admpub/bindata/v3 v3.1.5 // indirect
	github.com/admpub/confl v0.1.0
	github.com/admpub/copier v0.0.2 // indirect
	github.com/admpub/decimal v0.0.2 // indirect
	github.com/admpub/errors v0.8.2
	github.com/admpub/events v1.2.0 // indirect
	github.com/admpub/fsnotify v1.5.0 // indirect
	github.com/admpub/go-reuseport v0.0.4 // indirect
	github.com/admpub/humanize v0.0.0-20190501023926-5f826e92c8ca // indirect
	github.com/admpub/log v1.3.2
	github.com/admpub/map2struct v0.1.0 // indirect
	github.com/admpub/realip v0.0.0-20210421084339-374cf5df122d // indirect
	github.com/admpub/regexp2 v1.1.7
	github.com/araddon/gou v0.0.0-20190110011759-c797efecbb61 // indirect
	github.com/denisenkom/go-mssqldb v0.11.0
	github.com/francoispqt/gojay v1.2.13 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/goccy/go-json v0.7.10 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lib/pq v1.10.4
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.7.0
	github.com/webx-top/captcha v0.0.1 // indirect
	github.com/webx-top/com v0.3.3
	github.com/webx-top/echo v2.22.23+incompatible
	github.com/webx-top/pagination v0.1.1
	github.com/webx-top/poolx v0.0.0-20210912044716-5cfa2d58e380 // indirect
	github.com/webx-top/tagfast v0.0.0-20161020041435-9a2065ce3dd2
	github.com/webx-top/validation v0.0.3 // indirect
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871 // indirect
	golang.org/x/net v0.0.0-20211123203042-d83791d6bcd9 // indirect
	golang.org/x/sys v0.0.0-20211123173158-ef496fb156ab // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.4.0 // indirect
	modernc.org/ql v1.4.0
)
